const categories = [];
const clues = [];
const targetElement = document.querySelector("main")
let playerScore = 0;
const myNewGrid = new JeopardyGrid({ rowCount: 6, colCount: 6, target: targetElement })
myNewGrid.generateGrid()
const ScoreId = document.getElementById("score")

Promise.all([getCategories(), getClues()])
    .then(([categories, clues]) => {
        myNewGrid.populateGrid(categories, clues);
    })

// async function makeRequests () {
//     const [categories, clues] = await Promise.all(getCategories(), getClues())
//     myNewGrid.populateGrid(categories, clues)
// }

function scoreUpdate(){
    ScoreId.innerHTML = playerScore;
}

function click(question, value, answer){
    const questionPrompt = prompt(question)
    switch (questionPrompt) {
        case answer:
          playerScore += value;
          scoreUpdate()
          alert("That is correct!")
          
          break;
        default:
          alert("That is incorrect!")
    }





}
