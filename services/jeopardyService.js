const categoryIds = [253, 555, 579, 80, 141, 19664]
const categoriesUrl = 'https://jservice.xyz/api/categories/'
const cluesUrl = 'https://jservice.xyz/api/clues/?category='


function getCategories(){
    const promisesOfCategoryObjects = categoryIds.map(id => {
        return fetch(categoriesUrl + id).then(response => response.json())
    })
    
    return Promise.all(promisesOfCategoryObjects)
}

function getClues(){
    const promisesOfClueObjects = categoryIds.map(id => {
        return fetch(cluesUrl + id).then(response => response.json())
    })
    
    return Promise.all(promisesOfClueObjects)
}