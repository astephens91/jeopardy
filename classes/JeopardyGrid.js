
class JeopardyGrid extends GenericGrid {
    constructor(options){
        super(options)
    }
    createCell (rowIndex, colIndex){
        return new JeopardyCell ({
            rowIndex, 
            colIndex, 
            width: this.cellWidth, 
            height: this.cellHeight
        })
    }


    

    populateGrid (categories, clues) {
        let cellActive = true;
        const catNames = ["FASHION DESIGNERS", "PAC-MAN", "FIRST BORN", "SPORTS PEOPLE WHO ARE PLACES", "THE PURPLE TESTAMENT", "SONG VERBS"];
        for (let rowIndex in this.gridModel) {
            const row = this.gridModel[rowIndex];
            //let cells = document.getElementsByClassName(row);
            // The above line might work if Ryan didn't haplessly abandon his students in their time of need! -Morgan
            for (let colIndex in row) {
                const cell = row[colIndex]
                const clue = clues[colIndex][rowIndex]
                const cellId = document.getElementById(rowIndex + "-" + colIndex )
                const catId = document.getElementById(5 + "-" + colIndex)
                if (rowIndex != 5){
                cell.answer = clue.answer
                cell.question = clue.question
                cell.value = clue.value
                cell.category = clue.category.title
                console.log(cell)
                cellId.innerHTML = cell.value
                
                cellId.addEventListener("click", e => {click(cell.question, cell.value, cell.answer)})}
                
                else{
                    catId.innerHTML = catNames[colIndex]
                }
           
                
                
            }
        }

    }

}