class GenericGrid {
    constructor ({ rowCount = 4, colCount = 4, cellHeight = 100, cellWidth = 100,  target = document.body}){
        this.rowCount = rowCount;
        this.colCount = colCount;
        this.cellWidth = cellWidth;
        this.cellHeight = cellHeight;
        this.target = target;
        this.gridModel = [];
    }

    generateGrid () {
        for (let rowIndex = 0; rowIndex < this.rowCount; rowIndex += 1){
            const rowElement = this.createRowElement(rowIndex)
            this.gridModel.push([])
            
            
            for (let colIndex = 0; colIndex < this.colCount; colIndex += 1){
                const cell = this.createCell(rowIndex, colIndex);
                this.gridModel[rowIndex].push(cell);
                rowElement.appendChild(cell.element);
                cell.id = colIndex + "-" +rowIndex
            }
        }
        console.log(this)
    }
    createCell (rowIndex, colIndex){
        //override me if you have a child element
        return new GenericCell ({
            rowIndex, 
            colIndex, 
            width: this.cellWidth, 
            height: this.cellHeight
        })
    }
    createRowElement(rowIndex){
        const rowElement = document.createElement("div")
        rowElement.dataset.rowIndex = rowIndex;
        rowElement.style.height = this.cellHeight + "px"
        rowElement.classList.add("row");
        this.target.appendChild(rowElement);

        return rowElement
    }
}