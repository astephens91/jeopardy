class GenericCell {
    constructor ({ rowIndex, colIndex, width, height}){
        this.rowIndex = rowIndex;
        this.colIndex = colIndex;
        this.width = width;
        this.height = height;
        this.generateCell()
    }

    generateCell(){
        this.element = document.createElement("div")

        //cell instance provides a point-back to constructor
        this.element.cellInstance = this;

        
        this.element.dataset.rowIndex = this.rowIndex;
        this.element.dataset.colIndex = this.colIndex;
        this.element.style.width = this.width + "px"
        this.element.style.height = this.height + "px"
        this.element.classList.add("cell")
        this.element.id = this.rowIndex + "-" + this.colIndex
        
            
        
    }
}